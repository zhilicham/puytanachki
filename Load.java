package Puytanachki;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;
public class Load {
    public static void LoadTag(String path) throws IOException {
        File file = new File(path);
        if (file.length() == 0) {
            System.out.println("Selected slot is empty,please select other slot");
            Start.Start("l");
        } else {
            Scanner scn = new Scanner(file);
            ArrayList<String[]> nums = new ArrayList<>();
            while (scn.hasNext()) {
                nums.add(scn.nextLine().split(" "));
            }
            int columns = nums.get(0).length;
            int[][] z = new int[nums.size()][columns];
            Iterator<String[]> iter = nums.iterator();
            for (int i = 0; i < z.length; i++) {
                String[] s = iter.next();
                for (int j = 0; j < columns; j++) {
                    z[i][j] = Integer.parseInt(s[j]);
                }
            }
            for (int[] a : z) {
                for (int b : a) {
                    if (b >= 10) {
                        System.out.print("  " + b);

                    } else {
                        System.out.print("   " + b);
                    }
                }
                System.out.println();
            }
            scn.close();
            Game.StartGame(z);
        }
    }
}
