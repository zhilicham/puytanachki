package Puytanachki;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Game {
    public static void StartGame(int[][] z) throws IOException {
        int[][] p = new int[4][4];
        int f = 1;
        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p[i].length; j++) {
                p[i][j] = f;
                f++;
            }
        }
        p[3][3] = 0;
        System.out.println();
        //Перемещение пятнашкм
        Scanner scanner = new Scanner(System.in);
        do {
            int k = 0, l = 0;
            System.out.println("Enter the tag number to move\nEnter n for a new game.\nEnter s to save.\nEnter l to load.\nEnter q to exit.");
            if (scanner.hasNext("[^qlsn(0-9)]")) {
                System.out.println("You entered wrong character");
                break;
            } else {
                if (scanner.hasNext("n")) {
                    System.out.println("New game command introduced.");
                    String key = "n";
                    Start.Start(key);
                    break;

                }
                if (scanner.hasNext("q")) {
                    System.out.println("Quit command introduced.");
                    String key = "q";
                    Start.Start(key);
                    break;

                }
                if (scanner.hasNext("s")) {
                    System.out.println("Save command introduced.");
                    String key = "s";
                    StartToSave.Start2(z);
                    break;
                }

                if (scanner.hasNext("l")) {
                    System.out.println("Load command introduced.");
                    String key = "l";
                    Start.Start(key);
                    break;

                }
                if (scanner.hasNextInt()) {
                    int input = scanner.nextInt();
                    for (int i = 0; i < z.length; i++) {
                        for (int j = 0; j < z[i].length; j++) {
                            if (z[i][j] == input) {
                                k = i;
                                l = j;
                            }
                        }
                    }
                    if (l > 0) {
                        if (z[k][l - 1] == 0) {
                            z[k][l] = 0;
                            z[k][l - 1] = input;
                        }

                    }
                    if (l < 3) {
                        if (z[k][l + 1] == 0) {
                            z[k][l] = 0;
                            z[k][l + 1] = input;
                        }

                    }
                    if (k > 0) {
                        if (z[k - 1][l] == 0) {
                            z[k][l] = 0;
                            z[k - 1][l] = input;
                        }

                    }
                    if (k < 3) {
                        if (z[k + 1][l] == 0) {
                            z[k][l] = 0;
                            z[k + 1][l] = input;
                        }

                    }
                    for (int[] a : z) {
                        for (int b : a) {
                            if (b < 10) {
                                System.out.print("  " + b + " ");

                            } else {
                                System.out.print(" " + b + " ");
                            }
                        }
                        System.out.println();
                    }
                }
            }
        }
        while (!Arrays.deepEquals(p, z));
        if (Arrays.deepEquals(p, z)) {
            System.out.println("Congratulations you won.");

        }
    }
}
