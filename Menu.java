package Puytanachki;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
public class Menu {
    public static void main(String[] args) throws IOException {
        System.out.println("Hi let's play tag.");
        System.out.println("In order to select one of the menu items, enter the letter in brackets from the keyboard.");
        System.out.println("new game (n)");
        System.out.println("load game (l)");
        System.out.println("quit game (q) ");
        Scanner scanner = new Scanner(System.in);
        String key = scanner.nextLine();
        Start.Start(key);
    }
}
class StartToSave{
    public  static  void Start2(int z[][]) throws IOException {
        int key2;
        System.out.println("Choose a slot to save, from one to five");
        Scanner scanner1 = new Scanner(System.in);
        key2 = scanner1.nextInt();
        switch (key2) {
            case 1:
                Save.SaveTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot1",z);
                break;
            case 2:
                Save.SaveTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot2",z);
                break;
            case 3:
                Save.SaveTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot3",z);
                break;
            case 4:
                Save.SaveTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot4",z);
                break;
            case 5:
                Save.SaveTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot5",z);
                break;
            default:
                System.out.println("You entered wrong slot number...\n");
                break;
        }

    }
}
class Start {
    public static void Start(String key) throws IOException {
        switch (key) {
            case "n":
                NewGame.createTag(4);
                break;
            case "l":
                int key3;
                System.out.println("Choose a slot to load, from one to five");
                Scanner scanner2 = new Scanner(System.in);
                key3 = scanner2.nextInt();
                switch (key3) {
                    case 1:
                        Load.LoadTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot1");
                        break;
                    case 2:
                        Load.LoadTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot2");
                        break;
                    case 3:
                        Load.LoadTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot3");
                        break;
                    case 4:
                        Load.LoadTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot4");
                        break;
                    case 5:
                        Load.LoadTag("C:\\Users\\Александр\\IdeaProjects\\Upr\\NaZhachet\\src\\Puytanachki\\Slot5");
                        break;
                    default:
                        System.out.println("You entered wrong slot number...\n");
                }
                break;
            case "q":
                System.out.println("Ending program...");
                break;
            default:
                System.out.println("You entered wrong menu item...\n");
        }

    }
}
    class NewGame {
        public static void createTag(int size) throws IOException {
            boolean in_array = true;
            int numb;
            int[][] z = new int[size][size];
            System.out.println();
            for (int i = 0; i < z.length; i++) {
                Arrays.fill(z[i], -1);
            }
            for (int i = 0; i < z.length; i++) {
                for (int j = 0; j < z[i].length; j++) {
                    do {
                        in_array = false;
                        numb = (int) (Math.random() * 16);
                        for (int k = 0; k <= i; k++) {
                            for (int l = 0; l < z.length; l++) {
                                if ((int) z[k][l] == (int) numb) {
                                    in_array = true;
                                    break;
                                }
                            }
                        }
                    } while (in_array);
                    z[i][j] = numb;
                }
            }
            System.out.println("Game started.");
            System.out.println(" ");
            for (int[] a : z) {
                for (int b : a) {
                    if (b < 10) {
                        System.out.print("  " + b + " ");

                    } else {
                        System.out.print(" " + b + " ");
                    }
                }
                System.out.println();
            }
            System.out.println(" ");
            Game.StartGame(z);
        }
    }
